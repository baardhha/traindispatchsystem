package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * This class represents a train departure.
 * A train departure has a departure time, line, train number, destination, track and delay.
 * departureTime is input as a String and,
 * and later in the constructor converted to LocalTime for easier use.
 * line is input as a String in order to be able to use both letters and numbers.
 * trainNumber is input as an int, simply because it is a number.
 * destination is a String because a place name could consist of both letters and numbers,
 * especially in bus, tram or train stations.
 * Example. "Lerkendal 2".
 * track is an int because it is a number.
 * delay is an int because the delay is most effectively measured in whole minutes,
 * and therefore does not need to be a double, or any other type.
 * The only variables that have setters are track and delay, because they might
 * need to be changed after the object is created in order for the station to
 * arrange the logistics around the train departure.
 *
 * @author Bård Helge Hansen
 * @version 1.0
 * @since 0.1
 */
public class TrainDeparture {
  private final String departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private int delay;

  /**
   * Constructor for train departure.
   * departureTime is input as a String and in the parameters in the constructor,
   * but then converted to LocalTime for easier use, and better readability for the user.
   */
  public TrainDeparture(String departureTime, String line, int trainNumber,
                        String destination, int track, int delay) throws IllegalArgumentException {
    if (departureTime == null || departureTime.isBlank()) {
      throw new IllegalArgumentException("\nERROR: Departure time cannot be null or blank.");
    }
    if (line == null || line.isBlank()) {
      throw new IllegalArgumentException("\nERROR: Line cannot be null or blank.");
    }
    if (trainNumber < 1) {
      throw new IllegalArgumentException("\nERROR: Train number cannot be negative.");
    }
    if (destination == null || destination.isBlank()) {
      throw new IllegalArgumentException("\nERROR: Destination cannot be null or blank.");
    }
    if (track < 0) {
      throw new IllegalArgumentException("\nERROR: Track cannot be negative.");
    }
    if (delay < 0) {
      throw new IllegalArgumentException("\nERROR: Delay cannot be negative.");
    }
    this.departureTime = LocalTime.parse(departureTime).toString();
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  public LocalTime getDepartureTime() {
    return LocalTime.parse(departureTime);
  }

  public String getLine() {
    return line;
  }

  public int getTrainNumber() {
    return trainNumber;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public int getDelay() {
    return delay;
  }

  /**
   * set-method for changing the assigned track to a departure.
   *
   * @param track New track which the user would like to assign departure to.
   *
   * @throws IllegalArgumentException when the track is less than 0.
   *                                  Track = 0 means no track assigned.
   *                                  A negative track is not allowed.
   */
  public void setTrack(int track)
      throws IllegalArgumentException {
    if (track < 0) {
      throw new IllegalArgumentException("\nERROR: Track cannot be negative.");
    }
    this.track = track;
  }

  /**
   * set-method for changing the delay to a departure.
   *
   * @param delay New delay which the user would like to assign to the departure
   * @throws IllegalArgumentException when delay is less than 0.
   *                                  Delay = 0 means no delay
   *                                  A negative delay is not allowed
   */
  public void setDelay(int delay) throws IllegalArgumentException {
    if (delay < 0) {
      throw new IllegalArgumentException("\nERROR: Delay cannot be negative.");
    }
    this.delay = delay;
  }

  @Override
  public String toString() {
    if (delay == 0 && track != 0) { // toString() called when a train departure has no delay.
      return "Departure time: " + departureTime
          + " | Line: " + line
          + " | Train number: " + trainNumber
          + " | Destination: " + destination
          + " | Track: " + track + "\n";
    }
    if (track == 0 && delay != 0) { // toString() called when a train departure has no track
      // assigned and has a delay.
      return "Departure time: " + departureTime
          + " | Line: " + line
          + " | Train number: " + trainNumber
          + " | Destination: " + destination
          + " | Delay: " + delay + " min.\n";
    }
    if (track == 0) { // toString() called when a train departure has no track assigned.
      return "Departure time: " + departureTime
          + " | Line: " + line
          + " | Train number: " + trainNumber
          + " | Destination: " + destination + "\n";
    }
    // toString() called when a train departure has both a delay and a track assigned.
    return "Departure time: " + departureTime
        + " | Line: " + line
        + " | Train number: " + trainNumber
        + " | Destination: " + destination
        + " | Delay: " + delay + " min."
        + " | Track: " + track + "\n";
  }
}

