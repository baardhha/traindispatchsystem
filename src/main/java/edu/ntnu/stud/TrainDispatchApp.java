package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 *
 * @author Bård Helge Hansen
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {
  /**
   * main method.
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();

    ui.init();
    ui.start();
  }
}
