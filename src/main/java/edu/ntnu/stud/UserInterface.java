package edu.ntnu.stud;

import java.util.Scanner;

/**
 * This is the interface class for the train dispatch application.
 * The class contains a scanner for user input, and a TrainDepartureOverview object.
 * The init method is used to initialize the application with some train departures.
 *
 * @author Bård Helge Hansen
 * @version 1.0
 * @since 0.1
 */
public class UserInterface {
  Scanner scanner = new Scanner(System.in);
  TrainDepartureOverview overview = new TrainDepartureOverview();

  /**
   * Method for initializing the application with some train departures (for testing).
   */
  public void init() {
    TrainDeparture departure1 = new TrainDeparture("16:00", "L1", 1, "Bergen", 1, 0);
    TrainDeparture departure2 = new TrainDeparture("12:00", "L2", 2, "Trondheim", 2, 5);
    TrainDeparture departure3 = new TrainDeparture("14:30", "L3", 3, "Oslo", 3, 0);
    TrainDeparture departure4 = new TrainDeparture("10:00", "L4", 4, "Stavanger", 4, 7);

    overview.getTrainDepartures().add(departure1);
    overview.getTrainDepartures().add(departure2);
    overview.getTrainDepartures().add(departure3);
    overview.getTrainDepartures().add(departure4);
  }

  private void showMenu() {
    System.out.println("\n__________Welcome to the Train Dispatch Application__________");
    System.out.println("Please input one of the following options to continue \n");
    System.out.println(" '1' - View all departures.");
    System.out.println(" '2' - Register a new departure.");
    System.out.println(" '3' - Assign a departure to a track.");
    System.out.println(" '4' - Add a delay to a departure.");
    System.out.println(" '5' - Find a departure by train number.");
    System.out.println(" '6' - Search for departures with same destination.");
    System.out.println(" '7' - Update current time.");
    System.out.println(" '9' - Close application");
    System.out.println("\n Please enter an option below:");
  }

  private void backToMainMenu() {
    System.out.println("Press '9' to return to the main menu.");
    while (true) {
      if (scanner.hasNextInt()) {
        int returnToMenu = scanner.nextInt();
        if (returnToMenu == 9) {
          break;
        } else {
          System.out.println("Input is not a valid option. Please try again.");
        }
      } else {
        System.out.println("Input is not a valid option. Please try again.");
        scanner.next();
      }
    }
  }

  private void viewAllUi() {
    System.out.println("__________Departures__________");
    boolean showDepartures = true;
    while (showDepartures) {
      System.out.println(overview.allTrainDepartures());
      backToMainMenu();
      showDepartures = false;
    }
  }

  private void registerNewDepartureUi() {
    System.out.println("Enter departure time (HH:MM): ");
    final String departureTime = scanner.next();
    scanner.nextLine();
    if (departureTime.matches("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$")) {
      System.out.println("Enter line: ");
      if (scanner.hasNext()) {
        final String line = scanner.nextLine();
        System.out.println("Enter train number: ");
        if (scanner.hasNextInt()) {
          final int trainNumber = scanner.nextInt();
          System.out.println("Enter destination: ");
          if (scanner.hasNext()) {
            final String destination = scanner.next();
            scanner.nextLine();
            System.out.println("Enter track (press Enter to skip): ");
            int track = 0;
            if (scanner.hasNextLine()) {
              String trackInput = scanner.nextLine().trim();
              if (!trackInput.isEmpty()) {
                try {
                  track = Integer.parseInt(trackInput);
                } catch (NumberFormatException e) {
                  System.out.println("ERROR: Input is not a valid track number. Please try again.");
                  return;
                }
              } else {
                System.out.println("Skipping track assignment.");
              }
            }
            System.out.println("Enter delay (in whole minutes)(press Enter to skip): ");
            int delay = 0;
            if (scanner.hasNextLine()) {
              String delayInput = scanner.nextLine().trim();
              if (!delayInput.isEmpty()) {
                try {
                  delay = Integer.parseInt(delayInput);
                } catch (NumberFormatException e) {
                  System.out.println("ERROR: Input is not a valid delay. Please try again.");
                  return;
                }
              } else {
                System.out.println("Adding no delay.");
              }
            }
            boolean answer = overview.newTrainDeparture(departureTime, line, trainNumber,
                destination, track, delay);
            if (answer) {
              System.out.println("Train departure is registered!");
            } else {
              System.out.println("Failed to register new train departure. Please try again.");
            }
          } else {
            System.out.println("ERROR: Input is not a valid destination. Please try again.");
          }
        } else {
          System.out.println("ERROR: Input is not a valid train number. Please try again.");
        }
      } else {
        System.out.println("ERROR: Input is not a valid line. Please try again.");
      }
    } else {
      System.out.println("ERROR: Input is not a valid departure time. Please try again.");
    }
  }

  private void assignDepartureUi() {
    System.out.println("Enter train number: ");
    if (scanner.hasNextInt()) {
      int assignTrackTrainNumber = scanner.nextInt();
      TrainDeparture departure = overview.findTrainDeparture(assignTrackTrainNumber);
      if (departure == null) {
        System.out.println("Departure with train number does not exist.");
      } else {
        System.out.println(departure);
        System.out.println("Assign track number (enter 0 to unassign to current track): ");
        if (scanner.hasNextInt()) {
          int newTrackNumber = scanner.nextInt();
          boolean trackAlreadyAssigned = overview.getTrainDepartures().stream()
              .anyMatch(trainDeparture -> trainDeparture.getTrack() == newTrackNumber);
          if (trackAlreadyAssigned) {
            System.out.println("Another departure is already assigned to that track.");
          } else {
            departure.setTrack(newTrackNumber);
            System.out.println("Assigning train number '" + assignTrackTrainNumber
                + "' to track '" + newTrackNumber + "'.");
          }
        } else {
          System.out.println("Input is not a valid track number. Please try again.");
          scanner.next();
        }
      }
    } else {
      System.out.println("Input is not a valid train number. Please try again.");
      scanner.next();
    }
  }

  private void addDelayUi() {
    System.out.println("Enter train number: ");
    if (scanner.hasNextInt()) {
      int delayTrainNumber = scanner.nextInt();
      TrainDeparture departure = overview.findTrainDeparture(delayTrainNumber);
      if (departure == null) {
        System.out.println("Departure with train number does not exist.");
      } else {
        System.out.println(departure);
        System.out.println("Enter delay (in whole minutes): ");
        if (scanner.hasNextInt()) {
          int newDelay = scanner.nextInt();
          departure.setDelay(newDelay);
          System.out.println("Delay for train number '" + delayTrainNumber
              + "' is now '" + newDelay + "' minutes.");
        } else {
          System.out.println("Input is not a valid delay. Please try again.");
          scanner.next();
        }
      }
    } else {
      System.out.println("Input is not a valid train number. Please try again.");
      scanner.next();
    }
  }

  private void findByTrainNumberUi() {
    System.out.println("Enter train number: ");
    if (scanner.hasNextInt()) {
      int findTrainNumber = scanner.nextInt();
      TrainDeparture departure = overview.findTrainDeparture(findTrainNumber);
      if (departure == null) {
        System.out.println("Departure with train number does not exist.");
      } else {
        System.out.println("__________Departure_________");
        boolean showDeparture = true;
        while (showDeparture) {
          System.out.println(departure);
          backToMainMenu();
          showDeparture = false;
        }
      }
    } else {
      System.out.println("Input is not a valid train number. Please try again.");
      scanner.next();
    }
  }

  private void searchForSameDestinationUi() {
    System.out.println("Enter destination: ");
    String destination = scanner.next();
    boolean showSameDestination = true;
    while (showSameDestination) {
      System.out.println(overview.sameDestination(destination));
      backToMainMenu();
      showSameDestination = false;
    }
  }

  private void updateCurrentTimeUi() {
    System.out.println("Enter new time (HH:MM): "
        + "\n(Current time is " + overview.getCurrentTime() + ") ");
    String newCurrentTime = scanner.next();
    if (newCurrentTime.matches("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$")) {
      if (overview.setCurrentTime(newCurrentTime)) {
        System.out.println("Current time is now " + overview.getCurrentTime() + ".");
        overview.removeDeparturedTrains();
      }
    } else {
      System.out.println("\nInput is not a valid time. Please try again.");
    }
  }

  /**
   * Method for starting the application.
   * The method shows the menu, and asks for user input.
   * The method then runs the method that corresponds to the user input.
   * The method runs until the user inputs 9, which closes the application.
   * If the user inputs something else than a valid option, the method asks for user input again.
   */
  public void start() {
    boolean run = true;
    while (run) {
      showMenu();
      if (scanner.hasNextInt()) {
        int menuChoice = scanner.nextInt();
          { switch (menuChoice) {
            case 1:
              viewAllUi();
              break;
            case 2:
              registerNewDepartureUi();
              break;
            case 3:
              assignDepartureUi();
              break;
            case 4:
              addDelayUi();
              break;
            case 5:
              findByTrainNumberUi();
              break;
            case 6:
              searchForSameDestinationUi();
              break;
            case 7:
              updateCurrentTimeUi();
              break;
            case 9:
              System.out.println("Closing application.");
              run = false;
              break;
            default:
              System.out.println(menuChoice + " is not a valid option. Please try again.");
            }
          }
      } else {
        System.out.println("Input is not a valid option. Please try again.");
        scanner.next();
      }
    }
  }
}
