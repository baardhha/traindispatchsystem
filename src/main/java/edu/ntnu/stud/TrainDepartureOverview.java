package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Class for storing and registering train departures, as well as the current time.
 * The current time is initially a string (same as in the TrainDeparture class),
 * and later parsed to LocalTime.
 * The train departures are stored in an ArrayList for easier sorting by departure time.
 *
 * @author Bård Helge Hansen
 * @since 0.1
 * @version 1.0
 */
public class TrainDepartureOverview {
  private String currentTime = "00:00"; // Automatically set to 00:00 (a new day).
  private final ArrayList<TrainDeparture> trainDepartures;

  public TrainDepartureOverview() {
    this.trainDepartures = new ArrayList<>();
    this.currentTime = LocalTime.parse(currentTime).toString();
  }

  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  public String getCurrentTime() {
    return LocalTime.parse(currentTime).toString();
  }

  /**
   * set-method for changing the new current time.
   *
   * @param currentTime String-input parsed to LocalTime and compared to this.currentTime.
   * @return True if new currentTime is after the old currentTime, false if not.
   */
  public boolean setCurrentTime(String currentTime) {
    try {
      if (LocalTime.parse(currentTime).isBefore(LocalTime.parse(this.currentTime))) {
        System.out.println("\nERROR: New current time cannot be before the old current time.");
        return false;
      }
      this.currentTime = currentTime;
      return true;
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  /**
   * Method for adding a new train departure to the ArrayList trainDepartures.
   * The method checks if the train number already exists in the ArrayList.
   * If it does, the method returns false, and the train departure is not added.
   * If it does not, the method returns true, and the train departure is added.
   *
   * @param departureTime is input as a String and parsed to LocalTime.
   * @param line is the line the train departure belongs to. String.
   * @param trainNumber is the train number of the train departure. int.
   * @param destination is the destination of the train departure. String.
   * @param track is the track the train departure is departing from. int.
   * @param delay is the delay of the train departure. int.
   *
   * @return True if the train departure is added, false if not.
   */
  public boolean newTrainDeparture(String departureTime, String line, int trainNumber,
                                   String destination, int track, int delay) {
    try {
      TrainDeparture newTrainDeparture = new TrainDeparture(departureTime, line, trainNumber,
          destination, track, delay);
      boolean trainNumberExists = this.trainDepartures.stream()
          .anyMatch(trainDeparture -> trainDeparture.getTrainNumber() == trainNumber);
      if (trainNumberExists) {
        return false;
      } else {
        this.trainDepartures.add(newTrainDeparture);
        return true;
      }
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  /**
   * Method for finding a train departure by train number.
   * If the train number does not exist, the method returns null.
   * If the train number exists, the method returns the train departure.
   *
   * @param trainNumber is the train number of the train departure. int.
   * @return The train departure if it exists, null if not.
   */
  public TrainDeparture findTrainDeparture(int trainNumber) {
    for (TrainDeparture trainDeparture : this.trainDepartures) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        return trainDeparture;
      }
    }
    return null;
  }

  /**
   * Method for finding all train departures with the same destination.
   * The method returns a string with all the train departures with the same destination.
   * If there are no train departures with the same destination, the method returns a string
   * saying that there are no train departures with that destination.
   * The method sorts the train departures by departure time.
   *
   * @param destination is the destination of the train departures. String.
   * @return String with all the train departures with the same destination.
   */
  public String sameDestination(String destination) {
    ArrayList<TrainDeparture> sameDestination = new ArrayList<>();
    for (TrainDeparture trainDeparture : this.trainDepartures) {
      if (trainDeparture.getDestination().equals(destination)) {
        sameDestination.add(trainDeparture);
      }
    }
    // This piece of code is AI-generated.
    sameDestination.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    StringBuilder sameDestinationString = new StringBuilder();
    for (TrainDeparture trainDeparture : sameDestination) {
      sameDestinationString.append(trainDeparture.toString());
    }
    if (sameDestinationString.toString().isBlank()) {
      return "No departures with that destination.";
    } else {
      return sameDestinationString.toString();
    }
    // Generated code ends here.
  }

  /**
   * Method for removing all train departures that have departed.
   * The method iterates through the ArrayList trainDepartures and checks if the departure time
   * plus the delay time is before the current time.
   * If it is, the train departure is removed from the ArrayList.
   */
  public void removeDeparturedTrains() {
    // This piece of code is AI-generated.
    Iterator<TrainDeparture> iterator = trainDepartures.iterator();
    while (iterator.hasNext()) {
      TrainDeparture trainDeparture = iterator.next();
      LocalTime departureTime = trainDeparture.getDepartureTime();
      Duration delayTime = Duration.ofMinutes(trainDeparture.getDelay());
      LocalTime departureTimeWithDelay = departureTime.plus(delayTime);
      if (departureTimeWithDelay.isBefore(LocalTime.parse(currentTime))) {
        iterator.remove();
      }
    }
    // Generated code ends here.
  }

  /**
   * Method for printing all train departures.
   * The method sorts the train departures by departure time.
   *
   * @return String with all the train departures.
   */
  public String allTrainDepartures() {
    this.trainDepartures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    StringBuilder allTrainDepartures = new StringBuilder();
    for (TrainDeparture trainDeparture : this.trainDepartures) {
      allTrainDepartures.append(trainDeparture.toString());
    }
    return allTrainDepartures.toString();
  }

  @Override
  public String toString() {
    return "TrainDepartureOverview"
        + "trainDepartures = " + trainDepartures;
  }
}
