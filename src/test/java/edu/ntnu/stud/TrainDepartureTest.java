package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureTest {
  private final String testDepartureTime = "10:00";
  private final String testLine = "L1";
  private final int testTrainNumber = 1;
  private final String testDestination = "test-destination";
  private final int testTrack = 1;
  private final int testDelay = 1;

  TrainDeparture testDeparture = new TrainDeparture(testDepartureTime, testLine, testTrainNumber, testDestination, testTrack, testDelay);
  @Test
  void getDepartureTime() {
    assertEquals("10:00", testDeparture.getDepartureTime().toString());
  }

  @Test
  void getLine() {
    assertEquals("L1", testDeparture.getLine());
  }

  @Test
  void getTrainNumber() {
    assertEquals(1, testDeparture.getTrainNumber());
  }

  @Test
  void getDestination() {
    assertEquals("test-destination", testDeparture.getDestination());
  }

  @Test
  void getTrack() {
    assertEquals(1, testDeparture.getTrack());
  }

  @Test
  void getDelay() {
    assertEquals(1.0, testDeparture.getDelay());
  }

  @Test
  void setTrack() {
    testDeparture.setTrack(2);
    assertEquals(2, testDeparture.getTrack());
  }

  @Test
  void setTrackNegative() {
    assertThrows(IllegalArgumentException.class, () -> testDeparture.setTrack(-1));
  }

  @Test
  void setTrackZero() {
    testDeparture.setTrack(0);
    assertEquals(0, testDeparture.getTrack());
  }

  @Test
  void setDelay() {
    testDeparture.setDelay(2);
    assertEquals(2, testDeparture.getDelay());
  }

  @Test
  void setDelayZero() {
    testDeparture.setDelay(0);
    assertEquals(0, testDeparture.getDelay());
  }

  @Test
  void setDelayNegative() {
    assertThrows(IllegalArgumentException.class, () -> testDeparture.setDelay(-1));
  }
}