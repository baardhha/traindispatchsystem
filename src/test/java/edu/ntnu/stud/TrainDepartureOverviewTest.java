package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureOverviewTest {
  TrainDepartureOverview testOverview = new TrainDepartureOverview();
  private void testOverview() {
    TrainDeparture testDeparture1 = new TrainDeparture("10:00", "L1", 1, "test-destination", 1, 1);
    TrainDeparture testDeparture2 = new TrainDeparture("12:00", "L2", 2, "test-destination", 1, 1);
    testOverview.getTrainDepartures().add(testDeparture1);
    testOverview.getTrainDepartures().add(testDeparture2);
  }
  
  @Test
  void getTrainDeparturesWithDepartures() {
    testOverview();
    TrainDeparture testDeparture1 = testOverview.getTrainDepartures().get(0);
    TrainDeparture testDeparture2 = testOverview.getTrainDepartures().get(1);
    List<TrainDeparture> expectedDepartures = Arrays.asList(testDeparture1, testDeparture2);
    List<TrainDeparture> actualDepartures = testOverview.getTrainDepartures();
    assertEquals(expectedDepartures, actualDepartures);
  }

  @Test
  void getTrainDeparturesWithoutDepartures() {
    List<TrainDeparture> expectedDepartures = new ArrayList<>();
    List<TrainDeparture> actualDepartures = testOverview.getTrainDepartures();
    assertEquals(expectedDepartures, actualDepartures);
  }

  @Test
  void getCurrentTime() {
    assertEquals("00:00", testOverview.getCurrentTime());
  }

  @Test
  void setCurrentTimeCorrect() {
    testOverview.setCurrentTime("01:00");
    assertEquals("01:00", testOverview.getCurrentTime());
  }

  @Test
  void setCurrentTimeEarlier() {
    testOverview.setCurrentTime("09:00");
    testOverview.setCurrentTime("08:00");
    assertFalse(testOverview.setCurrentTime("08:00"));
    assertEquals("09:00", testOverview.getCurrentTime());
  }

  @Test
  void newTrainDepartureNormal() {
    assertTrue(testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureTrainNumberExists() {
    testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", 1, 1);
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureDepartureTimeNull() {
    assertFalse(testOverview.newTrainDeparture(null, "L1", 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureDepartureTimeBlank() {
    assertFalse(testOverview.newTrainDeparture("", "L1", 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureLineNull() {
    assertFalse(testOverview.newTrainDeparture("10:00", null, 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureLineBlank() {
    assertFalse(testOverview.newTrainDeparture("10:00", "", 1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureTrainNumberNegative() {
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", -1, "test-destination", 1, 1));
  }

  @Test
  void newTrainDepartureDestinationNull() {
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", 1, null, 1, 1));
  }

  @Test
  void newTrainDepartureDestinationBlank() {
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", 1, "", 1, 1));
  }

  @Test
  void newTrainDepartureTrackNegative() {
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", -1, 1));
  }

  @Test
  void newTrainDepartureDelayZero() {
    assertTrue(testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", 1, 0));
  }

  @Test
  void newTrainDepartureDelayNegative() {
    assertFalse(testOverview.newTrainDeparture("10:00", "L1", 1, "test-destination", 1, -1));
  }

  @Test
  void findExistingTrainDeparture() {
    testOverview();
    TrainDeparture expectedDeparture = testOverview.getTrainDepartures().get(0);
    TrainDeparture actualDeparture = testOverview.findTrainDeparture(1);
    assertEquals(expectedDeparture, actualDeparture);
  }

  @Test
  void findNonExistingTrainDeparture() {
    testOverview();
    TrainDeparture actualDeparture = testOverview.findTrainDeparture(3);
    assertNull(actualDeparture);
  }

  @Test
  void noDeparturesWithSameDestination() {
    String noDeparturesWithThatDestination = testOverview.sameDestination("non-existing-destination");
    assertEquals("No departures with that destination.", noDeparturesWithThatDestination);
  }

  @Test
  void removeNoDeparturedTrains() {
    testOverview();
    testOverview.removeDeparturedTrains();
    assertEquals(2, testOverview.getTrainDepartures().size());
  }

  @Test
  void removeOneDeparturedTrains() {
    testOverview();
    testOverview.setCurrentTime("11:00");
    testOverview.removeDeparturedTrains();
    assertEquals(1, testOverview.getTrainDepartures().size());
  }

  @Test
  void removeAllDeparturedTrains() {
    testOverview();
    testOverview.setCurrentTime("23:59");
    testOverview.removeDeparturedTrains();
    assertEquals(0, testOverview.getTrainDepartures().size());
  }

  @Test
  void allTrainDepartures() {
    testOverview.allTrainDepartures();
  }
}
