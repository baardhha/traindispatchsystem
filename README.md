# Portfolio project IDATA1003 - 2023

STUDENT NAME = Bård Helge Hansen  
STUDENT ID = 10052

## Project description
[//]: # (TODO: Write a short description of your project/product here.)
This project is a train dispatch system. It is a system where you have a variety of choices in order to administrate train departures for a single train station.
The reason why this train dispatch system is made is that it is a requirement for the course IDATT1003 in the Norwegian University of Science and Technology. The system is made in Java and is made with the help of the IDE IntelliJ IDEA.
## Project structure
[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
The project is split into six different classes. Four classes for the application, and two additional classes for testing. The classes are as follows:
- TrainDeparture.java
- TrainDepartureOverview.java
- UserInterface.java
- TrainDispatchApp.java
- TrainDepartureTest.java
- TrainDepartureOverviewTest.java

## Link to repository
[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/baardhha/traindispatchsystem

## How to run the project
[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
The application is run by running the TrainDispatchApp class, or the main method within the class.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
The two test-classes is made for testing TrainDeparture and TrainDepartureOverview. The way you could run the tests is by either running the whole class, (You would then run all the test-methods in that class at once), or if you would like to only test one specific method, you can open the class, find the right method, and run this for itself.

## References
- "Objects first with Java", Sixth edition, by Barnes and Kölling, ISBN ....
- GutHub CoPilot - https://copilot.github.com/
- https://chat.openai.com/

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
